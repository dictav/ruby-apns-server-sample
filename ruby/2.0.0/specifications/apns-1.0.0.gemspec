# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name = "apns"
  s.version = "1.0.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.authors = ["James Pozdena"]
  s.autorequire = "apns"
  s.date = "2010-03-22"
  s.description = "Simple Apple push notification service gem"
  s.email = "jpoz@jpoz.net"
  s.extra_rdoc_files = ["MIT-LICENSE"]
  s.files = ["MIT-LICENSE"]
  s.homepage = "http://github.com/jpoz/apns"
  s.require_paths = ["lib"]
  s.rubygems_version = "2.0.3"
  s.summary = "Simple Apple push notification service gem"

  if s.respond_to? :specification_version then
    s.specification_version = 3

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<rspec>, [">= 0"])
    else
      s.add_dependency(%q<rspec>, [">= 0"])
    end
  else
    s.add_dependency(%q<rspec>, [">= 0"])
  end
end
