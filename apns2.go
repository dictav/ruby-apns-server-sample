package main

import (
	"fmt"
	apns "github.com/anachronistic/apns"
	"time"
)

func main() {
	payload := apns.NewPayload()
	payload.Alert = "Hello, world!"
	payload.Badge = 42
	payload.Sound = "default"

	pn := apns.NewPushNotification()
	pn.DeviceToken = "88a26d9399e1d352ba1f97d085bd8eed99f3d719b10511d760f40ade0a29b572"
	pn.AddPayload(payload)

	client := apns.NewClient("gateway.sandbox.push.apple.com:2195", "cert.pem", "key.pem")
	resp := client.Send(pn)

	alert, _ := pn.PayloadString()
	fmt.Println("  Alert:", alert)
	fmt.Println("Success:", resp.Success)
	fmt.Println("  Error:", resp.Error)

	for count := 10; count > 0; count-- {
		time.Sleep(1 * 1000 * 1000 * 1000)
		fmt.Println(count)
	}
	resp = client.Send(pn)

	alert, _ = pn.PayloadString()
	fmt.Println("  Alert:", alert)
	fmt.Println("Success:", resp.Success)
	fmt.Println("  Error:", resp.Error)

	fmt.Println("done")

}
