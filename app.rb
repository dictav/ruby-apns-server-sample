require 'apns'

# gateway.sandbox.push.apple.com is default
#APNS.host = 'gateway.push.apple.com' 

#APNS.pem  = './Certificates.pem'
APNS.pem  = './cert.pem'

APNS.port = 2195 

#device_token = '88a26d9399e1d352ba1f97d085bd8eed99f3d719b10511d760f40ade0a29b572'
#device_token = '3f3965a6cb3f6a723fc797dc9e22356a58dbbeb85e50adaecc63d701c6eebd8c'
#device_token = 'dbc1d07eb0bf6c7c125a55c481ed3be326f7ec7d3c710b71f5ea4de373795c1c'
#
device_token = 'dbc1d07eb0bf6c7c125a55c481ed3be326f7ec7d3c710b71f5ea4de373795c1c'
#APNS.send_notification(device_token, 'Hello iPhone!' )

p APNS.send_notification(device_token,
												 :alert => { "loc-key" => "Comment",
												 "loc-args" => [ "Jenna","Hello"] },
												 :badge => "2",
												 :sound => 'default')
